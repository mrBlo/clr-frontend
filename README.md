# Complete Login & Registration Frontend 

## Table of contents
* [General Info](#markdown-header-general-info)
* [Description](#markdown-header-description)
* [App Demo](#markdown-header-app-demo)
* [App Screenshot](#markdown-header-app-screenshot)
* [Technologies](#markdown-header-technologies)
* [Features](#markdown-header-features)


### General Info 
This project is a **ReactJS** powered application and was bootstrapped with Create React App.


### Description 
This project serves pages for a complete Login and Registration flow.
 
+ This project is the frontend for my`CLR app` (Java Spring Boot). You can check out the backend in my repo.
+ This project is for *demo* purposes and is pubished on netlify.
+ VERSION: v 1.0


### App Demo 
* Hosted on [https://myclr.netlify.app/](https://myclr.netlify.app/)


### App Screenshot
![diagram](./LoginPage.png)


### Technologies 
Project is created with:

* React JS 
* Axios - for REST calls
* Bootstrap
* React Hook Form
* Redux - for State management
* Toastify - for toasts


### Features 
 The project for now has the following features:
 
* Registration
* Login Authentication
* Forgot Password functionality (implemented with mailtrap)


### User Stories 
 The project for now has the following: 

* As a visitor, I want to register so that my user account will be activated.
* As a registered user, I want to login so that I can access my dahsboard.
* As a registered user, I want to be able to reset my password via email.


### Future Additions
* Signup Activation Token functionality 


### Notable: 
#### React Router Fix 
* Fixing React Router errors in Netlify
(Fix)[https://dev.to/dance2die/page-not-found-on-netlify-with-react-router-58mc](https://dev.to/dance2die/page-not-found-on-netlify-with-react-router-58mc)


#### CRA Fix 
* Enable Continuous Deployment build in Netlify
```
"build": "CI= react-scripts build",
```

### Who do I talk to? 
* Repo owner : `isaac.afrifa3@yahoo.com`

