
export const HOME_URL = "/";
export const LOGIN_URL = "/login";
export const SIGNUP_URL = "/signup";
export const DASHBOARD_URL = "/dashboard";
export const FORGOTPASSWORD_URL = "/forgot-password";
export const RESETPASSWORD_URL = "/reset-password";
export const BACKEND_URL = "https://completelr.herokuapp.com";
//https://completelr.herokuapp.com
//http://localhost:8080
