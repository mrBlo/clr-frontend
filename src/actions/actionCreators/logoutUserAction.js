import { ActionTypes } from '../actionTypes'

//Creating a function that returns an action
export const removeLoggedInUserAction = () => {
    return {
        //Action with type and additional payload 
        type: ActionTypes.REMOVE_LOGGEDIN_USER,
        payload: {} //setting user stae in store to {}

    }
}