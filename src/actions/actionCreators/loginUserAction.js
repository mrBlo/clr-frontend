import { ActionTypes } from '../actionTypes'

//Creating a function that returns an action
export const setLoginUserAction= (user) => {
    return {
        //Action with type and additional payload 
        type: ActionTypes.SET_LOGGEDIN_USER,
        payload: user
         
    }
}