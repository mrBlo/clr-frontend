import { ActionTypes } from '../actionTypes'

//Creating a function that returns an action
export const setLoginStatusAction = () => {
    return {
        //Action with type and additional payload 
        type: ActionTypes.SET_LOGIN_STATUS_TO_TRUE,
        payload: 'LOGGED_IN'

    }
}