import { ActionTypes } from '../actionTypes'

//Creating a function that returns an action
export const setLogoutStatusAction = () => {
    return {
        //Action with type and additional payload 
        type: ActionTypes.SET_LOGIN_STATUS_TO_FALSE,
        payload: 'NOT_LOGGED_IN'

    }
}