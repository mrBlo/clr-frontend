/*
Using redux-persist to persist state to localStorage 
*/
import { createStore} from 'redux';
import rootReducer from '../reducers/rootReducer'

//Redux-persist Imports
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web browser
 
// configuration object for redux-persist
const persistConfig = {
    key: 'root',
    storage, // define which storage to use
  }

  const persistedReducer = persistReducer(persistConfig, rootReducer)

  //commented out cos I'm passing a persistedReducer to createStore instead of a normal 'rootReducer'
 //creating Store with reducer
// export const store= createStore(rootReducer);

 const store = createStore(persistedReducer)
 const persistor = persistStore(store); // used to create the persisted store

 export {store, persistor}
