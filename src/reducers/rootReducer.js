import { ActionTypes } from '../actions/actionTypes'

//create Initial State
const initialState = {
    loggedInUser: {},
    loginStatus: "NOT_LOGGED_IN"
}

const rootReducer = (state = initialState, action) => {
    // console.log(action)
    if (action.type === ActionTypes.SET_LOGIN_STATUS_TO_TRUE) {
        return {
            ...state, //using the spread operator to avoid overriding the overall state
            loginStatus: action.payload //update only the loginStatus state
        }
    }

    if (action.type === ActionTypes.SET_LOGGEDIN_USER) {
        // console.log("User : "+JSON.stringify(action.payload))
        return {
            ...state,
            loggedInUser: action.payload
        }
    }

    if (action.type === ActionTypes.SET_LOGIN_STATUS_TO_FALSE) {
        return {
            ...state,
            loginStatus: action.payload
        }
    }

    if (action.type === ActionTypes.REMOVE_LOGGEDIN_USER) {
        return {
            ...state,
            loggedInUser: action.payload
        }
    }
    return state;
}

export default rootReducer;