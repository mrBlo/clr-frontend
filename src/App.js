import React, { Component } from 'react';
import './assets/css/App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { HOME_URL, LOGIN_URL, SIGNUP_URL, DASHBOARD_URL, FORGOTPASSWORD_URL, RESETPASSWORD_URL } from "./constants/urls";
import Home from "./components/Home"
import ForgotPassword from "./components/ForgotPassword";
import Login from "./components/auth/Login";
import SignUp from "./components/auth/SignUp";
import Dashboard from "./components/private/Dashboard";
import ResetPassword from "./components/ResetPassword";
import Page404 from "./components/Page404";
import PrivateRoute from "./components/PrivateRoute";
import NavigationBar from './components/NavigationBar';
import { connect } from "react-redux";
import { setLoginStatusAction } from './actions/actionCreators/loginStatusAction'
import { setLoginUserAction } from './actions/actionCreators/loginUserAction'
import { CookiesProvider } from 'react-cookie'; //allows you to read the CSRF cookie and send it back as a header


class App extends Component {

  constructor() {
    super();

    // this.state = {
    //   loggedInStatus: "NOT_LOGGED_IN",
    //   user: {}
    // }
    this.handleLogin = this.handleLogin.bind(this);
  }

  //this method is passed into Login component as a prop
  //  handleLogin(data) {
  //   this.setState({
  //     loggedInStatus: "LOGGED_IN",
  //     user: data
  //   });
  // }

 
  //this method is passed into Login component as a prop
  handleLogin(data) {
    this.props.setLoginStatus() //setLoginStatus is a function which acts as an action dispatch
    this.props.setLoginUser(data)
  }

  render() {
    return (
      <CookiesProvider>
      <Router>
        <div className="App">
       
          {/* NAVIGATION BAR */}
          {/* passing loggedInStatus to be used in NavigationBar component */}
          <NavigationBar loggedInStatus={this.props.storeLogInStatus} />
          
          <Switch>
            {/* HOME ROUTE */}
            <Route exact path={HOME_URL}
              render={props => (
                <Home
                  //using props to pass status to Home component
                  {...props}
                // loggedInStatus={this.props.storeLogInStatus}
                />
              )} />

            {/* LOGIN ROUTE */}
            <Route exact path={LOGIN_URL}
              // using render props
              render={props => (
                <Login
                  //using props to pass method to Login component
                  {...props}
                  handleLogin={this.handleLogin}
                />
              )}
            />
            {/* SIGN UP ROUTE */}
            <Route path={SIGNUP_URL}>
              <SignUp />
            </Route>

            {/* DASHBOARD ROUTE */}
            <PrivateRoute
              authed={this.props.storeLogInStatus}
              path={DASHBOARD_URL}
              component={Dashboard} />

            {/* using PrivateRoute so commented out below LOC */}
            {/* <Route
              exact path={DASHBOARD_URL}
              // using render props
              render={props => (
                <Dashboard
                  {...props}
                  // loggedInStatus={this.props.storeLogInStatus}
                  // user={this.props.storeLoggedInUser}
                />
              )}
            /> */}

            {/* FORGOT-PASSWORD ROUTE*/}
            <Route exact path={FORGOTPASSWORD_URL} component={ForgotPassword} />

            {/* RESET-PASSWORD ROUTE*/}
           <Route exact path={RESETPASSWORD_URL} component={ResetPassword} />

            {/* 404 ROUTE*/}
            {/* <Route exact path="*" component={() => "404 NOT FOUND"}  /> */}
            <Route exact path="*">
              <Page404 />
            </Route>
      
          </Switch>

        </div>
      </Router>
      </CookiesProvider>
    );
  }

}

/*  REDUX FUNCTIONS */
//Getting the Store state and mapping it to props for the App component to use
const mapStateToProps = (state) => {
  return {
    storeLogInStatus: state.loginStatus,
    storeLoggedInUser: state.loggedInUser
  }
}

//updating the Store state and dispatch to props
const mapDispatchToProps = (dispatch) => {
  return {
    setLoginStatus: () => {
      dispatch(setLoginStatusAction()) //dispatch an action here
    },
    setLoginUser: (user) => {
      dispatch(setLoginUserAction(user))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
