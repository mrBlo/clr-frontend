import React, { Component } from "react";
import "../../assets/css/App.css";
import { Link } from "react-router-dom";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.min.css';
import { connect } from 'react-redux'
import { compose } from 'redux'; //for multiple higher order components
import { withRouter } from 'react-router-dom'

import { Spinner } from 'react-bootstrap';

class Signup extends Component {
  constructor(props) {
    super(props);

    //setting state to represent user credentials and also to contain messages
    this.state = {
      email: "",
      username: "",
      password: "",
      confirm_password: "",
      passwordMatch: null,
      hasRegistrationError: false, //for when server doesnt produce 201 status code
      showSuccessMessage: false,
      hasError: false, //for when there is no response from server
      hasUserExistsError: false,
      isLoading: false
    };
    this.handleComparePassword = this.handleComparePassword.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this);
    // this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }

  //Redirect if already logged In
  componentDidMount() {
    if (this.props.storeLogInStatus === "LOGGED_IN") {
      alert("You cannot access page if you are already logged in!")
      this.props.history.push('/')
    }
  }

  //this method is passed into Login component as a prop
  handleSuccessfulAuth(data) {
    this.props.handleLogin(data); //passed from App component as a prop
    this.props.history.push("/dashboard");
  }

  //using events to update state as soon as values in the text fields change.
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
    //checking for password match
    if (event.target.name === "confirm_password" || event.target.name === "password") {
      this.handleComparePassword(event)
    }
  }

  //Toastify notification function
  notify() {
    toast.info(' Loading..');
  }


  handleComparePassword = (event) => {
    if (event.target.value === this.state.password || event.target.value === this.state.confirm_password) {
      this.setState({
        passwordMatch: true
      })
    } else {
      this.setState({
        passwordMatch: false
      })
    }
  }

  //resetting forms
  resetForms() {
    this.setState({
      email: "",
      username: "",
      password: "",
      confirm_password: "",
    });
  }

  //onSubmit function
  handleSubmit = (event) => {
    event.preventDefault();
    const { email, username, password, confirm_password } = this.state;

    // perform all neccassary validations
    if (password !== confirm_password) {
      this.setState({ showSuccessMessage: false });
      this.setState({ passwordMatch: false });
    }

    else {
      // var user_object = { //changed post input
      //   //  email: email,
      //   username: username,
      //   password: password,
      // };

      this.notify();
      this.setState({ isLoading: true });
      var user_object = {
        email: email,
        user: {
          username: username,
          password: password
        }
      };

      const options = {
        method: "POST",
        data: user_object,
        // url: "http://localhost:8080/registration",
        url: "https://completelr.herokuapp.com/registration",
        withCredentials: true, //sets cookie in browser
      };

      axios(options)
        .then((response) => {
          console.log(response);

          console.log(response.status);
          if (response.status === 201) {
            this.setState({ hasRegistrationError: false });
            this.setState({ hasError: false });
            this.setState({ hasUserExistsError: false });
            this.setState({ passwordMatch: null });
            this.setState({ showSuccessMessage: true });
            this.resetForms();
          } else {
            //registration failed ie. response.status !=201 or 409
            this.setState({ showSuccessMessage: false });
            this.setState({ hasError: false });
            this.setState({ hasUserExistsError: false });
            this.setState({ passwordMatch: null });
            this.setState({ hasRegistrationError: true });
          }

          this.setState({ isLoading: false });
        })
        .catch((error) => {
          // Another Error like Network error occurred
          if (
            // eslint-disable-next-line eqeqeq
            error == "Error: Request failed with status code 409"
          ) {
            this.setState({ hasRegistrationError: false });
            this.setState({ hasError: false });
            this.setState({ showSuccessMessage: false });
            this.setState({ passwordMatch: null });
            this.setState({ hasUserExistsError: true });
          }
          else {
            this.setState({ showSuccessMessage: false });
            this.setState({ hasRegistrationError: false });
            this.setState({ hasUserExistsError: false });
            this.setState({ passwordMatch: null });
            this.setState({ hasError: true });
          }

          this.setState({ isLoading: false });
        });

    }
  };

  render() {
    return (
      <div className="container ">
        <div className="row justify-content-md-center">
          <div className="col-md-6">
            <div className="signup-form">
              <form className="text-center"
                onSubmit={this.handleSubmit.bind(this)}>

                <h2>Sign Up</h2>

                {/*  ERROR MSG */}
                {this.state.hasError && (
                  <div className="alert alert-danger">
                    <h6 className="alert-heading text-center">Error Occurred</h6>
                  </div>
                )}

                {/* REGISTRATION FAILED MSG */}
                {this.state.hasRegistrationError && (
                  <div className="alert alert-danger">
                    <h6 className="alert-heading text-center">Registration Failed</h6>
                  </div>
                )}

                {/* USER ALREADY EXISTS ERROR MSG */}
                {this.state.hasUserExistsError && (
                  <div className="alert alert-danger">
                    <h6 className="alert-heading text-center">Username Already Exists</h6>
                  </div>
                )}
                {/* REGISTRATION SUCCESS MSG */}
                {this.state.showSuccessMessage && (
                  <div className="alert alert-success text-center">
                    <h6 className="alert-heading">Registration Successful</h6>
                    <p>Your account is activated, kindly login.</p>
                  </div>
                )}

                {/* <h2 className="text-center">
              Status: 
            {this.props.loggedInStatus}
          </h2> */}

                <div className="form-group row">
                  <div className="col-12">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Username"
                      name="username"
                      required="required"
                      value={this.state.username}
                      onChange={this.handleChange}
                      minLength="6"
                      autoFocus
                    />
                  </div>
                </div>

                <div className="form-group row">

                  <div className="col-12">
                    <input
                      type="email"
                      className="form-control"
                      name="email"
                      placeholder="Email Address"
                      required="required"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="form-group row">
                  {/* <label className="col-form-label col-4">Password</label> */}
                  <div className="col-12">
                    <input
                      type="password"
                      className="form-control"
                      name="password"
                      placeholder="Password"
                      id="password"
                      required="required"
                      value={this.state.password}
                      onChange={this.handleChange}
                      minLength="8"
                    />
                  </div>
                </div>
                <div className="form-group row">

                  <div className="col-12">
                    <input
                      type="password"
                      className="form-control"
                      name="confirm_password"
                      id="confirm_password"
                      placeholder="Confirm Password"
                      required="required"
                      value={this.state.confirm_password}
                      onChange={this.handleChange}
                      //  onBlur={this.handleComparePassword}
                      minLength="8"
                    />
                    {/* Validity Feedback */}
                    {this.state.passwordMatch === false && (
                      <div className="text-danger">
                        <p>Passwords do not match</p>
                      </div>
                    )}


                  </div>
                </div>
                <div className="form-group row">
                  {/* <div className="col-10 offset-2"> */}
                  <div className="col-12 text-center">
                    <p>
                      <label className="form-check-label">
                        <input type="checkbox" required="required" /> I accept the{" "}
                        <Link to="/terms-of-use">Terms of Use</Link> &amp;{" "}
                        <Link to="/privacy-policy">Privacy Policy</Link>.
                  </label>
                    </p>
                    <button type="submit" className="btn btn-primary btn-lg">
                      Sign Up
                </button>
                  </div>
                </div>

                {/* Progress Loader */}
                <div className="text-center">
                  {this.state.isLoading && (
                    <Spinner animation="grow" variant="primary" />
                  )}
                </div>
              </form>

              <div className="text-center lowertext">
                Already have an account? <Link to="/login">Login here</Link>
              </div>
              {/* Toastify Container */}
              <ToastContainer className="text-center"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />

            </div>
          </div>
        </div>
      </div>
    );
  }
}
/*  REDUX FUNCTIONS */
//Getting the Store state and mapping it to props 
const mapStateToProps = (state) => {
  return {
    storeLogInStatus: state.loginStatus
  }
}

/* Commented out because I'm replacing with the compose function */
// using multiple higher order components 
//   export default withRouter(connect(mapStateToProps)(Signup));


//using compose function to chain multiple higher order components
const enhance = compose(
  // These are both single-argument HOCs
  withRouter,
  connect(mapStateToProps)
)
export default enhance(Signup);
