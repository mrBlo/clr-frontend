import axios from "axios";

/*
This function component sends requests to the backend to get the CSRF token
*/
const requestCsrfToken = () => {

  axios.get(
    // 'http://localhost:8080/api/csrf', 
   'https://completelr.herokuapp.com/api/csrf',
  { withCredentials: true }) //'withCredentials: true' to save cookie 
    .then((response) => {
      console.log(response)
    })
    .catch((error) => {
      console.log("Error: ", error.message);
    })
}

export default requestCsrfToken;


