import React, { Component } from "react";
import "../../assets/css/App.css";
import { Link } from "react-router-dom";
import ProfileImage from "../../assets/images/profile.png";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.min.css';
import axios from "axios";
import qs from "qs";
import { connect } from "react-redux";
import { Spinner } from 'react-bootstrap';
import { compose } from 'redux'; //for multiple higher order components
import { withCookies } from 'react-cookie'; // wraps the component at the bottom to give it access to cookies
//import requestCsrfToken from './requestCsrfToken'
import { BACKEND_URL } from "../../constants/urls";

class Login extends Component {

  constructor(props) {
    super(props);
   // const { cookies } = props;
   // console.log(`token: ${cookies.get('XSRF-TOKEN')}`);

    //setting state to represent user credentials and also to contain messages
    this.state = {
      username: "",
      password: "",
      hasLoginError: false,
      hasInvalidCredentials: false,
      isUserDisabled: false,
      loginCount: 0,
      isLoading: false,
      // csrfToken: cookies.get('XSRF-TOKEN'),
      // showSuccessMessage: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this);
  }


  componentDidMount() {
    //Redirect if already logged In
    if (this.props.storeLogInStatus === "LOGGED_IN") {
      alert("You cannot login if you are already logged in!")
      this.props.history.push('/')
    }
    //Sending GET request to get CSRF token
   // requestCsrfToken(); //imported externally

  }


  //Toastify notification function
  notify() {
    toast.info('Loading..');
  }

  //using events to update state as soon as values in the text fields change.
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }
  //resetting form
  resetForms() {
    this.setState({
      username: "",
      password: ""
    });
  }

  handleSuccessfulAuth(data) {
    this.props.handleLogin(data); //passed from App component as a prop
    this.props.history.push("/dashboard"); //.history is by default, part of the props passed in the Router render method ie.{...props}
  }


  //onSubmit function
  handleSubmit = (event) => {
    event.preventDefault();

    this.notify();
    this.setState({ isLoading: true });
    var user_object = {
      username: this.state.username,
      password: this.state.password,
    };

    const axiosParams = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded" //sending as form data not as JSON(which is the default)
        // , "X-XSRF-TOKEN" : this.state.csrfToken //commented out because browser automatically sends CSRF token to backend as header
      },
      data: qs.stringify(user_object), //converting object to form-urlencoded
      url: BACKEND_URL+"/login",
      withCredentials: true, //sets cookie in browser
    };

    axios(axiosParams)
      .then((response) => {
        console.log(response);
        if ( // response.request.responseURL === "http://localhost:8080/login_success"
          response.data !== "" && response.data != null
        ) {
          this.setState({ hasLoginError: false });
          this.setState({ hasInvalidCredentials: false });
          this.setState({ isUserDisabled: false });

          //deserializing (response.data) to object
          let authenticatedUser = {
            username: response.data.username,
            authority: response.data.authority,
            //authority: response.data.roleList[0].authority
          };
          //calling handleSuccessfulAuth function 
          this.handleSuccessfulAuth(authenticatedUser);
          this.resetForms();
        }
        else {
          //catching any error with a valid axios response
          this.setState({ isUserDisabled: false });
          this.setState({ hasInvalidCredentials: false });
          this.setState({ hasLoginError: true });
        }

        this.setState({ isLoading: false });
      })
      .catch((error) => {
        console.log("Error Response: ", error.response);
        console.log("Error Message: ", error.message);

        //LOGIN FAILED CHECK
        // eslint-disable-next-line eqeqeq
        if (error == "Error: Request failed with status code 401") {
          if (error.response.data.Exception === "Bad credentials") {
            this.setState({ hasLoginError: false });
            this.setState({ isUserDisabled: false });
            this.setState({ hasInvalidCredentials: true });
          } else if (error.response.data.Exception === "User is disabled") {
            this.setState({ hasLoginError: false });
            this.setState({ hasInvalidCredentials: false });
            this.setState({ isUserDisabled: true });
          } else {
            this.setState({ isUserDisabled: false });
            this.setState({ hasInvalidCredentials: false });
            this.setState({ hasLoginError: true });
          }
        }
        // Another Error like Network error occurred
        else {
          this.setState({ isUserDisabled: false });
          this.setState({ hasInvalidCredentials: false });
          this.setState({ hasLoginError: true });
        }

        this.setState({ isLoading: false });
      });
  };

  render() {
    //refactored to avoid using this.state.username and password
    const { username, password } = this.state;

    return (
      <div className="container ">
        <div className="row justify-content-md-center ">
          <div className="col-md-6">
            <div className="login-form">
              <form onSubmit={this.handleSubmit.bind(this)}>
                <div className="avatar">
                  <img
                    className="img-fluid"
                    src={ProfileImage}
                    alt="Avatar"
                  />
                </div>
                <h2 className="text-center">User Login</h2>

                {/* INVALID CREDENTIALS MSG */}
                {this.state.hasInvalidCredentials && (
                  <div className="alert alert-danger">
                    <h6 className="alert-heading text-center">Invalid Credentials</h6>
                  </div>
                )}
                {/* DISABLED MSG */}
                {this.state.isUserDisabled && (
                  <div className="alert alert-danger">
                    <h6 className="alert-heading text-center">Account is deactivated</h6>
                  </div>
                )}
                {/* LOGIN FAILED MSG */}
                {this.state.hasLoginError && (
                  <div className="alert alert-danger">
                    <h6 className="alert-heading text-center">Error Occurred</h6>
                  </div>
                )}

                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    name="username"
                    placeholder="Username"
                    required="required"
                    value={username}
                    onChange={this.handleChange}
                    minLength="6"
                    autoFocus
                  />
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    name="password"
                    placeholder="Password"
                    required="required"
                    value={password}
                    onChange={this.handleChange}
                    minLength="6"
                  />
                </div>

                <div className="form-group">
                  <button
                    type="submit"
                    onSubmit={this.loginButtonClicked}
                    className="btn btn-primary btn-lg btn-block"
                  >
                    Sign in
              </button>
                </div>
                {/* Progress Loader */}
                <div className="text-center">
                  {this.state.isLoading && (
                    <Spinner animation="border" variant="info" />
                  )}
                </div>

                <div className="bottom-action clearfix">
                  {/* <label className="float-left form-check-label">
                    <input type="checkbox" /> Remember me
              </label> */}
                  <Link to="/forgot-password" className="float-right">
                    Forgot Password?
              </Link>
                </div>
              </form>
              <p className="text-center lowertext">
                Don't have an account? <Link to="/signup">Sign up here!</Link>
              </p>

              {/* Toastify Container */}
              <ToastContainer className="text-center"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

/*  REDUX FUNCTIONS */
//Getting the Store state and mapping it to props 
const mapStateToProps = (state) => {
  return {
    storeLogInStatus: state.loginStatus
  }
}

//export default (connect(mapStateToProps)(Login))
//export default withCookies(connect(mapStateToProps)(Login));

//using compose function to chain multiple higher order components
const enhance = compose(
  // These are both single-argument HOCs
  withCookies,
  connect(mapStateToProps)
)
export default enhance(Login);
