import React, { Component } from "react";
import { Container, Row, Col } from 'react-bootstrap'
import { connect } from "react-redux";
import axios from "axios";
import { setLogoutStatusAction } from '../actions/actionCreators/logoutStatusAction'
import { removeLoggedInUserAction } from '../actions/actionCreators/logoutUserAction'
import { BACKEND_URL } from "../constants/urls";

class Home extends Component {

  handleLogout = () => {
    this.props.setLogoutStatus() //sets store's loginStatus to "NOT_LOGGED_IN"
    this.props.removeLoggedInUser()
  }

  handleLogoutClick = () => {
    const options = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      }, //sending as form data not as JSON(which is the default)
      url: BACKEND_URL + "/logout",
      withCredentials: true, //sets cookie in browser
    };

    axios(options)
      .then((response) => {
        console.log(response);
        this.handleLogout();
      })
      .catch((error) => {
        alert("Couldn't Log Out. " + error);
      });
  }

  render() {
    return (
      <div className="App">

        {/* Home Page Content */}
        <section className="app-title-block">
          <Container className="h-100">
            <Row>
              <Col className="lg-8 mx-auto app-title" style={{ marginTop: "10%" }}>
                <h1 className="mt-5 text-center app-title-header">CLR</h1>
                <h2 className="text-center app-title-footer">
                  Making Login & Registration easier
                </h2>

                {/*  showing loginStatus */}
                {/* <h4 className="text-center text-white bg-success">
                    Status : {this.props.storeLogInStatus}
                </h4> */}
                {this.props.storeLogInStatus === "NOT_LOGGED_IN" && (
                  <div className="login-msg">
                    <p className="text-center text-white"> Kindly login or register </p>
                  </div>
                )}
                <br /> <br /> <br />
              </Col>
            </Row>
          </Container>
        </section>

        {/* show if User is Logged In */}
        {this.props.storeLogInStatus === "LOGGED_IN" ?
          <div className="text-center">
            <button className="text-white btn-danger btn-lg " onClick={this.handleLogoutClick}>Logout</button>
          </div>
          : ''}

      </div>
    );
  }
};

//Getting the Store state and mapping it to props for the component to use
const mapStateToProps = (state) => {
  return {
    storeLogInStatus: state.loginStatus
  }
}

/*  
 Commented out below LOCs because I'm not using ownProps,
 I'm only using the store state
 */
// const mapStateToProps = ( state, ownProps ) =>{ //ownProps are the original props of the component
//   return {
//     originalLoginStatusProps: ownProps.loggedInStatus,
//     storeLogInStatus : state.loginStatus
//   }
// }

//updating the Store state and dispatch to props
const mapDispatchToProps = (dispatch) => {
  return {
    setLogoutStatus: () => {
      dispatch(setLogoutStatusAction())
    },
    removeLoggedInUser: () => {
      dispatch(removeLoggedInUserAction())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);