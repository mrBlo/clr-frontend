import React from "react";
import { useState, useEffect } from "react";
import { useForm } from 'react-hook-form';

import { Link } from "react-router-dom";
import { LOGIN_URL, BACKEND_URL } from "../constants/urls";
import { Container, Spinner } from 'react-bootstrap';
import axios from "axios";


const ResetPassword = (props) => {

  const [hasError, setHasError] = useState(false)
  const [hasInvalidToken, setHasInvalidToken] = useState(false)
  const [isSuccessful, setIsSuccessful] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  //Using React Hook Form 
  const { register, handleSubmit, getValues, reset, errors } = useForm(); // initialize the hook here


  const getTokenParam = () => {
    const urlParams = new URLSearchParams(props.location.search);
    const tokenParam = urlParams.get("token");
    return tokenParam;
  }

  //check validity of token at initial render
  useEffect(() => {
    //Get token param
    let tokenParam = getTokenParam();

    axios.get(
      BACKEND_URL + '/reset_password/check_token',
      {
        params: { token: tokenParam }
      })
      .then((response) => {
        console.log(response)
        //checking for only invalid tokens 
        if (response.data.message === "UNSUCCESSFUL_OPERATION") {
          throw new Error(response.data.error); //throws either Invalid Reset Token or Expired Reset Token error
        }
      })
      .catch((error) => {
        console.log("Error: ", error.message);
        if (error.message === "Invalid Reset Token") {
          alert("Oops! Reset token is invalid")
          props.history.push('/')
        }
        else if (error.message === "Expired Reset Token") {
          alert("Oops! Reset Token is expired")
          props.history.push('/')
        }
        else { //Network Error
          alert("An error occurred!")
          props.history.push('/')
        }
      })
  }, []);  /* "[]" trigger useEffect only during initial render */


  const resetStateMessages = () => {
    setHasError(false)
    setIsSuccessful(false)
    setHasInvalidToken(false)
  }


  const onSubmit = (data, e) => {
    e.target.reset(); // reset after form submit
    resetStateMessages()
    //show ProgressSpinner
    setIsLoading(true)

    const params = {
      method: "POST",
      url: BACKEND_URL + "/reset_password",
      params: {
        token: getTokenParam(),
        password: data.password
      }
    };
    //send to server
    axios(params)
      .then(function (response) {
        console.log(response);
        if (response.data.message === "SUCCESSFUL_OPERATION") {
          setIsSuccessful(true)
          /* setting one error at a time */
          setHasError(false)
          setIsLoading(false)
          setHasInvalidToken(false)
        }
        else {
          // throw error and go to catch block
          throw new Error(response.data.error); //throws Invalid Reset Token error OR Expired Reset Token OR Password Reset Failed 
        }
      })
      .catch(function (error) {
        // handle error  
        console.log(error)
        if (error.message === "Invalid Reset Token" || error.message === "Password Reset Failed" || error.message === "Expired Reset Token") {
          setHasInvalidToken(true)
          /* setting one error at a time */
          setHasError(false)
          setIsSuccessful(false)
          setIsLoading(false)
        }
        else {
          setHasError(true)
          /* setting one error at a time */
          setIsSuccessful(false)
          setIsLoading(false)
          setHasInvalidToken(false)
        }
      })
  };


  return (
    <div id="layoutAuthentication">
      <div id="layoutAuthentication_content">
        <main>
          <Container>

            <form onSubmit={handleSubmit(onSubmit)}>

              <div className="row justify-content-center">
                <div className="col-lg-5">
                  <div className="card shadow-lg border-0 rounded-lg mt-5">
                    <div className="card-header">
                      <h4 className="text-center font-weight-light my-4" style={{ color: "black" }}>Reset your password</h4></div>
                    <div className="card-body">
                      <div className="small mb-3 text-muted text-center"><h6>Kindly enter a new password for your account.</h6></div>

                      {/* hasError MSG*/}
                      {hasError && (
                        <div className="alert alert-danger">
                          <h6 className="alert-heading text-center">An error occurred!</h6>
                        </div>
                      )}
                      {/* hasInvalidTokenError MSG*/}
                      {hasInvalidToken && (
                        <div className="alert alert-danger">
                          <h6 className="alert-heading text-center">Oops! Something went wrong</h6>
                        </div>
                      )}
                      {/* isSuccessful Msg */}
                      {isSuccessful && (
                        <div className="alert alert-success">
                          <h6 className="alert-heading text-center">You have successfully changed your password.</h6>
                        </div>
                      )}
                      <div className="form-group">
                        <label className="small mb-1" style={{ color: "black" }}>Password</label>
                        <input className="form-control py-4"
                          name="password"
                          ref={register({ required: "Password is required!" })}
                          type="password" aria-describedby="passwordHelp"
                          placeholder="Enter your new password"
                          minLength="6"
                          autoFocus
                        />
                        {errors.password && (
                          <p className="text-center" style={{ color: "red" }}>{errors.password.message}</p>
                        )}
                      </div>

                      <div className="form-group">
                        <label className="small mb-1" style={{ color: "black" }}>Confirm Password</label>
                        <input className="form-control py-4"
                          name="passwordConfirmation"
                          ref={register({
                            required: "Please confirm password!",
                            validate: {
                              matchesPreviousPassword: value => {
                                const { password } = getValues();
                                return password === value || "Passwords should match!";
                              }
                            }
                          })}
                          type="password" aria-describedby="passwordHelp"
                          placeholder="Confirm your new password"
                          minLength="6"
                        />
                        {errors.passwordConfirmation && (
                          <p className="text-center" style={{ color: "red" }}>
                            {errors.passwordConfirmation.message}
                          </p>
                        )}
                      </div>

                      <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                        <Link to={LOGIN_URL} className="float-right">Return to login </Link>
                        <button type="submit"
                          className="btn btn-primary"> Change Password </button>
                      </div>
                      <br />
                      {/* Show ProgressSpinner */}
                      {isLoading && (
                        <div className="text-center">
                          <Spinner animation="border" variant="info" />
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>

            </form>

          </Container>
        </main>
      </div>
    </div>


  );
}

export default ResetPassword;