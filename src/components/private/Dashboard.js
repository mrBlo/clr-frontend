import React, { Component } from "react";
import axios from "axios";
import { Container, Image } from 'react-bootstrap';
import ProfileImage from "../../assets/images/profile.png";
//Redux Imports
import { connect } from 'react-redux'
import { setLogoutStatusAction } from '../../actions/actionCreators/logoutStatusAction'
import { removeLoggedInUserAction } from '../../actions/actionCreators/logoutUserAction'
import { Spinner } from 'react-bootstrap';
import { BACKEND_URL } from "../../constants/urls";
//import { compose } from 'redux'; //for multiple higher order components
//import { withCookies } from 'react-cookie'; // wraps the component at the bottom to give it access to cookies
//import requestCsrfToken from '../auth/requestCsrfToken';


class Dashboard extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
    // const { cookies } = props;
    // console.log(`Token: ${cookies.get('XSRF-TOKEN')}`);

    this.state = {
      isLoading: false
    }
    this.handleLogout = this.handleLogout.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }

  componentDidMount() {
    //Sending GET request to get CSRF token
    // requestCsrfToken(); //imported externally
  }


  handleLogout() {
    this.props.setLogoutStatus() //sets store's loginStatus to "NOT_LOGGED_IN"
    this.props.removeLoggedInUser()
  }


  handleLogoutClick() {

    this.setState({ isLoading: true });
    const options = {
      method: "POST",
      headers: {
        "content-type": "application/x-www-form-urlencoded"
      }, //sending as form data not as JSON(which is the default)
      url: BACKEND_URL + "/logout",
      withCredentials: true, //sets cookie in browser
    };

    //don't use this,has state issues.Always use above approach
    // axios
    //   .post("http://localhost:8080/logout"
    //   , { withCredentials: true }
    //   )
    axios(options)
      .then((response) => {
        console.log(response);
        this.setState({ isLoading: false });
        this.handleLogout();
      })
      .catch((error) => {
        console.log("Error: " + error.message);
        alert("Could Not Log Out!");
        this.setState({ isLoading: false });
      });
  }

  render() {
    return (
      <div className="container ">
        <div className="row justify-content-md-center ">
          <div className="col-md-6">
            <div className="login-form">
              <Container>
                <div className="text-center text-white">
                  <h1>Dashboard</h1>
                  <div className="alert bg-success">
                    <h5>Status: {this.props.storeLogInStatus}</h5>
                  </div>
                </div>
                <div style={{ backgroundColor: "#f0f0f0" }}>
                  <Image
                    src={ProfileImage}
                    alt="Avatar"
                    fluid
                  />
                </div>

                <div className="alert alert-primary">
                  <h6 className="alert-heading text-center">Logged in as:
            <br />{JSON.stringify(this.props.storeLoggedInUser)}</h6>
                </div>
                <button className="text-white btn-danger btn-lg btn-block" onClick={this.handleLogoutClick}>Logout</button>
                {/* Progress Loader */}
                <div className="text-center">
                  {this.state.isLoading && (
                    <Spinner animation="border" variant="info" />
                  )}
                </div>
              </Container>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

/*  REDUX FUNCTIONS */
//Getting the Store state and mapping it to props for the App component to use
const mapStateToProps = (state) => {
  return {
    storeLogInStatus: state.loginStatus,
    storeLoggedInUser: state.loggedInUser
  }
}
//updating the Store state and dispatch to props
const mapDispatchToProps = (dispatch) => {
  return {

    setLogoutStatus: () => {
      dispatch(setLogoutStatusAction())
    },
    removeLoggedInUser: () => {
      dispatch(removeLoggedInUserAction())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

//using compose function to chain multiple higher order components
// const enhance = compose(
//   // These are both single-argument HOCs
//   withCookies,
//   connect(mapStateToProps, mapDispatchToProps)
// )
// export default enhance(Dashboard);