import React from "react";
import { useState } from "react";
import { useForm } from 'react-hook-form';

import { Link } from "react-router-dom";
import { LOGIN_URL, BACKEND_URL } from "../constants/urls";
import { Container, Spinner } from 'react-bootstrap';
import axios from "axios";


const ForgotPassword = () => {

  const [emailNotFoundError, setEmailNotFound] = useState(false)
  const [hasError, setHasError] = useState(false)
  const [isSuccessful, setIsSuccessful] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  //Using React Hook Form 
  const { register, handleSubmit, errors } = useForm(); // initialize the hook here


  const resetStateMessages = () => {
    setEmailNotFound(false)
    setHasError(false)
    setIsSuccessful(false)
  }

  const onSubmit = (data) => {
    resetStateMessages()
    //show ProgressSpinner
    setIsLoading(true)

    //console.log(data);
    //send email to server
    const params = {
      method: "POST",
      url: BACKEND_URL + "/forgot_password",
      params: {
        email: data.email
      }
    };

    axios(params)
      .then(function (response) {
        console.log(response);
        if (response.data.message === "SUCCESSFUL_OPERATION") {
          setIsSuccessful(true)
          /* setting one error at a time */
          setEmailNotFound(false)
          setHasError(false)
          setIsLoading(false)
        }
        else {
         // throw error and go to catch block
         throw new Error("Unsuccessful Operation");
        }
      })
      .catch(function (error) {
        // handle error  
        console.log(error)
      if(error.response){
         if (error.response.status === 404) {
            setEmailNotFound(true)
            /* setting one error at a time */
            setHasError(false)
            setIsSuccessful(false)
            setIsLoading(false)
        }
      }
        else {
          setHasError(true)
          /* setting one error at a time */
          setEmailNotFound(false)
          setIsSuccessful(false)
          setIsLoading(false)
        }
      })
  };


  return (
    <div id="layoutAuthentication">
      <div id="layoutAuthentication_content">
        <main>
          <Container>

            <form onSubmit={handleSubmit(onSubmit)}>

              <div className="row justify-content-center">
                <div className="col-lg-5">
                  <div className="card shadow-lg border-0 rounded-lg mt-5">
                    <div className="card-header">
                      <h3 className="text-center font-weight-light my-4" style={{ color: "black" }}>Password Recovery</h3></div>
                    <div className="card-body">
                      <div className="small mb-3 text-muted text-center">Enter your email address and we will send you 
                      an email with instructions for resetting your password.</div>
                      {/* EMAIL NOT FOUND MSG*/}
                      {emailNotFoundError && (
                        <div className="alert alert-danger">
                          <h6 className="alert-heading text-center">Email Address does not exist</h6>
                        </div>
                      )}
                      {/* hasError MSG*/}
                      {hasError && (
                        <div className="alert alert-danger">
                          <h6 className="alert-heading text-center">Something went wrong!</h6>
                        </div>
                      )}
                      {/* isSuccessful Msg */}
                      {isSuccessful && (
                        <div className="alert alert-success">
                          <h6 className="alert-heading text-center">We have sent you an email with a link to reset your password</h6>
                        </div>
                      )}
                      <div className="form-group">
                        <label className="small mb-1">Email</label>
                        <input className="form-control py-4"
                          name="email"
                          ref={register({ required: true })}
                          type="email" aria-describedby="emailHelp"
                          placeholder="Enter email address"
                          minLength="12"
                          autoFocus
                        />
                        {errors.email && (
                          <p className="text-center" style={{ color: "red" }}>Email is required</p>
                        )}
                      </div>
                      <div className="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                        <Link to={LOGIN_URL} className="float-right">Return to login </Link>
                        <button type="submit" className="btn btn-primary"> Reset Password </button>
                      </div>
                      {/* Show ProgressSpinner */}
                      {isLoading && (
                        <div className="text-center">
                          <Spinner animation="border" variant="info" />
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>

            </form>

          </Container>
        </main>
      </div>
    </div>


  );
}

export default ForgotPassword;