import React from "react";
import { ArrowLeft } from 'react-bootstrap-icons';
import { HOME_URL } from "../constants/urls";
import { Link } from "react-router-dom";
import Image404 from "../assets/images/error-404-monochrome.svg"

const Page404 = () => {
  return (
    <div id="layoutError">
    <div id="layoutError_content">
     <main>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-6">
                                <div className="text-center mt-4">
                                    <img className="mb-4 img-error" 
                                    alt="404 Avatar"
                                    src={Image404} />
                                    <p className="lead">This requested URL was not found on this server.</p>
                                    <Link to={HOME_URL}>
                                    {/* <a href="index.html"> </a>*/}
                                        <p className="lead_sec">
                                        <ArrowLeft color="#5cd3b4" className="mr-1" />
                                        Return to Home Page
                                      </p> 
                                      </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
    
                </div>
    </div>
  );
};

export default Page404;