import React, { Component } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap"; // Integration between React Router v4 and React Bootstrap.
import { HOME_URL, LOGIN_URL, SIGNUP_URL } from "../constants/urls";


class NavigationBar extends Component {

    render() {
        return (
            <Navbar expand="lg" variant="dark" bg="dark" fixed="bottom" >
                <Container>

                    <Navbar.Brand>
                        CLR
                     </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">

                        <Nav className="ml-auto">
                            {/* <Nav.Link href="/">Home</Nav.Link> */}
                            {/* <Nav.Link><NavLink to={HOME_URL}>Home</NavLink></Nav.Link> */}

                            <LinkContainer exact to={HOME_URL}>
                                <Nav.Link>Home</Nav.Link>
                            </LinkContainer>


                            {/* showing if Not LoggedIn */}
                            {this.props.loggedInStatus === "NOT_LOGGED_IN" ?
                                <LinkContainer exact to={LOGIN_URL}>
                                    <Nav.Link>Login</Nav.Link>
                                </LinkContainer>
                                : ''}

                            {this.props.loggedInStatus === "NOT_LOGGED_IN" ?
                                <LinkContainer exact to={SIGNUP_URL}>
                                    <Nav.Link>Register</Nav.Link>
                                </LinkContainer>
                                : ''}


                            {/* show if LoggedIn using loggedInStatus as props*/}
                            {this.props.loggedInStatus === "LOGGED_IN" ?
                                <LinkContainer to="/dashboard">
                                    <Nav.Link>Dashboard</Nav.Link>
                                </LinkContainer>
                                : ''}
                        </Nav>

                    </Navbar.Collapse>

                </Container>
            </Navbar>



        );

    }


}


export default NavigationBar;